package shop;

public class Product {
    public static Integer id_counter = 0;
    private String type;
    private String name;
    private Double price;
    private Integer id;

    public Product(String type, String name, Double price) {
        id_counter++;
        this.type = type;
        this.name = name;
        this.price = price;
        this.id = id_counter;
    }

    public Product(String type, String name, Double price, Integer id) {
        this.type = type;
        this.name = name;
        this.price = price;
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Product{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", id=" + id +
                '}';
    }

    public String found(){
        return "Product found\nType: " + type + "\nName: " + name + "\nPrice: " + price + "\nId: " + id + "\n";
    }

}
