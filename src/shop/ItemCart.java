package shop;

import java.util.ArrayList;
import java.util.List;

public class ItemCart {
    private List<Product> cart;
    private Integer cartAmount;
    private Double cartPrice;

    public ItemCart() {
        this.cart = new ArrayList<>();
        this.cartAmount = 0;
        this.cartPrice = 0.0;
    }

    public List<Product> getCart() {
        return cart;
    }

    public void setCart(List<Product> cart) {
        this.cart = cart;
    }

    public Integer getCartAmount() {
        return cartAmount;
    }

    public void setCartAmount(Integer cartAmount) {
        this.cartAmount = cartAmount;
    }

    public Double getCartPrice() {
        return cartPrice;
    }

    public void setCartPrice(Double cartPrice) {
        this.cartPrice = cartPrice;
    }

    public void addProduct(Product product){
        this.cart.add(product);
        this.cartAmount++;
        this.cartPrice+=product.getPrice();
    }

    public void removeProduct(Product product){
        this.cart.remove(product);
        this.cartAmount--;
        this.cartPrice -= product.getPrice();
    }

    public void removeAllProducts(){
        this.cart.clear();
        this.cartAmount = 0;
        this.cartPrice = 0.0;
    }
}
