package shop;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {
    public static boolean admin_logged;
    public static boolean user_logged;

    public static void main(String[] args) {
        List<User> users = new ArrayList<>();
        List<Product> products = new ArrayList<>();
        List<Product> productsFiltered = new ArrayList<>();
        Scanner scanner = new Scanner(System.in).useDelimiter("\\n");
        boolean status = true;
        while(status) {
            System.out.println("Please select option:");
            System.out.println("1 - log in; 2 - exit program");
            String select = scanner.next();
            switch(select){
                case "1":{
                    System.out.println("Enter user login:");
                    String uLogin = scanner.next();
                    System.out.println("Enter user password:");
                    String uPass = scanner.next();
                    try {
                        readUsersFromFile(users, "users.csv");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    User currentUser = null;
                    for (User userFromList : users) {
                        if (userFromList.getLogin().equals(uLogin) && userFromList.getPassword().equals(uPass)) {
                            currentUser = userFromList;
                            break;
                        }
                    }
                    if(currentUser !=null){
                        if(currentUser.getAdminRights()){
                            continueAsAdmin(products, currentUser, scanner);
                        } else {
                            continueAsUser(products, productsFiltered, currentUser, scanner);
                        }
                    } else {
                        System.err.println("Incorrect user credentials - user not found");
                        break;
                    }
                    break;
                }
                case "2":{
                    status = false;
                    break;
                }
                default:{
                    System.err.println("Incorrect option");
                    break;
                }
            }

        }
        scanner.close();
    }
    // Other methods:
    // Read users from file to list
    public static void readUsersFromFile(List<User> usersList, String filename) throws IOException {
        FileReader userDB = new FileReader(filename);
        BufferedReader buffer = new BufferedReader(userDB);
        while(true){
            String lineRead = buffer.readLine();
            if(lineRead == null){
                break;
            }
            String[] attributes = lineRead.split(", ");
            Boolean adminRights = Boolean.parseBoolean(attributes[3]);
            usersList.add(new User(attributes[0], attributes[1], attributes[2], adminRights));
        }
        userDB.close();
    }

    // Load products from file to list
    public static void readProductsFromFile(List<Product> productList, String filename) throws IOException {
        FileReader productsDB = new FileReader(filename);
        BufferedReader bufferProd = new BufferedReader(productsDB);
        while(true){
            String line = bufferProd.readLine();
            if(line == null){
                break;
            }
            String[] attributes = line.split(", ");
            Double tmp_price = Double.parseDouble(attributes[2]);
            Integer tmp_id = Integer.parseInt(attributes[3]);
            productList.add(new Product(attributes[0], attributes[1], tmp_price, tmp_id));
        }
        productsDB.close();
    }

    // Save products to file
    public static void writeProductsToFile (List<Product> productList, String filename) throws IOException {
        FileWriter fileWriter = new FileWriter(filename);
        for (Product tmpProduct : productList) {
            String tmp = tmpProduct.getType() + ", " + tmpProduct.getName() + ", " + tmpProduct.getPrice() + ", " + tmpProduct.getId() + "\n";
            fileWriter.write(tmp);
        }
        fileWriter.flush();
        fileWriter.close();
    }

    // Formatted view for products list
    public static void formattedProductListPrint(List<Product> productsPrint){
        int printIdCounter = 0;
        System.out.println("------------------------------------------------------------");
        System.out.printf("%3s %15s %15s %9s %2s", "#", "Type", "Name", "Price,$", "Id\n");
        System.out.println("------------------------------------------------------------");
        for(Product product: productsPrint){
            printIdCounter++;
            System.out.format("%3s %15s %15s %8.2f %2s",
                    printIdCounter, product.getType(), product.getName(), product.getPrice(), product.getId());
            System.out.println();
        }
    }

    // Formatted view for item cart
    public static void formattedCartPrint (ItemCart itemCart){
        if(itemCart.getCart().size()>0) {
            formattedProductListPrint(itemCart.getCart());
            System.out.format("Total amount: %2s\n", itemCart.getCartAmount());
            System.out.format("Total price: %8.2f\n", itemCart.getCartPrice());
        } else {
            System.out.println("Cart is empty - nothing to show!");
            System.out.println();
        }
    }

    // Method for adding product
    public static void addProduct(List<Product> productList, Scanner scanner){
        System.out.println("Enter new product type:");
        String product_type = scanner.next();
        System.out.println("Enter new product name:");
        String product_name = scanner.next();
        System.out.println("Enter new product price:");
        Double product_price = scanner.nextDouble();
        productList.add(new Product(product_type, product_name, product_price));
        System.out.println("New product successfully added with id = " + Product.id_counter);
    }

    // Method for changing product
    public static void changeProduct(List<Product> productList, Scanner scanner) {
        System.out.println("Enter id of product you want to change:");
        int id = scanner.nextInt();
        boolean productFound = false;
        for (Product product : productList) {
            if (product.getId() == id) {
                System.out.print(product.found());
                System.out.println("Product type is: " + product.getType() + "; Set new type, or press 'n' to skip this and move to next attribute");
                String new_type = scanner.next();
                if (new_type.equals("n")) {
                    System.out.println("skipped");
                } else {
                    product.setType(new_type);
                    System.out.println("New product type " + new_type + " set successfully!");
                }
                System.out.println("Product name is: " + product.getName() + "; Set new name, or press 'n' to skip this and move to next attribute");
                String new_name = scanner.next();
                if (new_name.equals("n")) {
                    System.out.println("skipped");
                } else {
                    product.setName(new_name);
                    System.out.println("New product name " + new_name + " set successfully!");
                }
                System.out.println("Product price is: " + product.getPrice() + "; Set new price, or type '0,00' to skip this and finish changing");
                double new_price = scanner.nextDouble();
                if (new_price == 0.00) {
                    System.out.println("skipped");
                } else {
                    product.setPrice(new_price);
                    System.out.println("New product price " + new_price + " set successfully!");
                }
                productFound = true;
            }
        }
        if (!productFound){
            System.err.println("Product with id=" + id + " was not found!");
            System.out.println();
        }
    }

    // Method for removing product
    public static void removeProduct(List<Product> productList, Scanner scanner){
        System.out.println("Enter id of product you want to remove from list:");
        boolean idFound = false;
        int id = scanner.nextInt();
        for(int i=0;i<productList.size();i++){
            if(productList.get(i).getId()==id){
                System.out.println(productList.get(i).found());
                productList.remove(i);
                System.out.println("Product successfully removed!");
                idFound = true;
            }
        }
        if(!idFound)
        {
            System.err.println("Product with id=" + id + " was not found!");
            System.out.println();
        }
    }

    // Method for log out as admin - Saving product list to file, update counter
    public static void logOutAsAdmin(List<Product> productList){
        admin_logged = false;
        try {
            writeProductsToFile(productList, "products.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (productList.size()>0){
            Product.id_counter = productList.get(productList.size() - 1).getId();
        } else {
            Product.id_counter = 0;
        }
        productList.clear();
    }

    // Method for filtering products
    public static void filterProducts(List<Product> productList, List<Product> productListFiltered, Scanner scanner){
        System.out.println("Please select filter option: 1 - by type; 2 - by name; 3 - by price; 4 - back to previous menu:");
        String filter_choice = scanner.next();
        switch (filter_choice) {
            case "1": {
                System.out.println("Select type of product to filter:");
                String typeF = scanner.next();
                productListFiltered = productList.stream().filter(product -> product.getType().equalsIgnoreCase(typeF)).collect(Collectors.toList());
                formattedProductListPrint(productListFiltered);
                productListFiltered.clear();
                break;
            }
            case "2": {
                System.out.println("Select part name of product to filter:");
                String nameF = scanner.next();
                productListFiltered = productList.stream().filter(product -> product.getName().toLowerCase().contains(nameF.toLowerCase())).collect(Collectors.toList());
                formattedProductListPrint(productListFiltered);
                productListFiltered.clear();
                break;
            }
            case "3": {
                System.out.println("Select price to filter: negative number - '<', positive number - '>':");
                double priceF = scanner.nextDouble();
                if (priceF > 0) {
                    for (Product product : productList) {
                        if (product.getPrice() > priceF) {
                            productListFiltered.add(product);
                        }
                    }
                    formattedProductListPrint(productListFiltered);
                    productListFiltered.clear();
                } else if (priceF < 0) {
                    for (Product product : productList) {
                        if (product.getPrice() < Math.abs(priceF)) {
                            productListFiltered.add(product);
                        }
                    }
                    formattedProductListPrint(productListFiltered);
                    productListFiltered.clear();
                } else {
                    System.err.println("0,00 price option isn't possible\n");
                    break;
                }
                break;
            }
            case "4":{
                break;
            }
            default:{
                System.err.println("Incorrect option\n");
                break;
            }
        }
    }

    // Method for filter loop
    public static void filterWorks(List<Product> productList, List<Product> productListFiltered, Scanner scanner) {
        boolean filter_works = true;
        while (filter_works) {
            System.out.println("Do you want to filter the list?(y - yes; n - back to previous menu):");
            String filter = scanner.next();
            if (filter.equals("y")) {
                filterProducts(productList, productListFiltered, scanner);
            } else if (filter.equals("n")) {
                filter_works = false;
                break;
            } else {
                System.err.println("Incorrect option\n");
                System.out.println();
                break;
            }
        }
    }

    // Method for finding products
    public static void searchForProduct(List<Product> productList, User currentUser, Scanner scanner){
        boolean searching_works = true;
        while (searching_works) {
            System.out.println("Please select how to search: 1 - by id; 2 - by name; 3 - exit to previous menu:");
            String search_option = scanner.next();
            switch (search_option) {
                case "1": {
                    System.out.println("Enter product id:");
                    boolean idFound = false;
                    int id_to_find = scanner.nextInt();
                    for (Product product : productList) {
                        if (product.getId() == id_to_find) {
                            System.out.println(product.found());
                            System.out.println("Press 'y' to add this product to cart, 'n' - back to the search:");
                            String adder = scanner.next();
                            idFound = true;
                            if (adder.equals("y")) {
                                currentUser.getUsercart().addProduct(product);
                            } else if (adder.equals("n")) {
                                break;
                            } else {
                                System.err.println("Incorrect option\n");
                                break;
                            }
                        }
                    }
                    if(!idFound)
                    {
                        System.err.println("Product with id=" + id_to_find + " was not found!");
                        break;
                    }
                    break;
                }
                case "2": {
                    System.out.println("Enter product name:");
                    boolean nameFound = false;
                    String name_to_find = scanner.next();
                    for (Product product : productList) {
                        if (product.getName().equalsIgnoreCase(name_to_find)) {
                            System.out.println(product.found());
                            System.out.println("Press 'y' to add this product to cart, 'n' - back to the search:");
                            String adder = scanner.next();
                            nameFound = true;
                            if (adder.equals("y")) {
                                currentUser.getUsercart().addProduct(product);
                                System.out.println("Product successfully added to Your cart!");
                                System.out.println();
                            } else if (adder.equals("n")) {
                                break;
                            } else {
                                System.err.println("Incorrect option\n");
                                break;
                            }
                        }
                    }
                    if(!nameFound)
                    {
                        System.err.println("Product with name: " + name_to_find + " was not found!");
                        break;
                    }
                    break;
                }
                case "3": {
                    searching_works = false;
                    break;
                }
                default:{
                    System.err.println("Incorrect option\n");
                    break;
                }
            }
        }
    }

    // Method for removing product by id
    public static void removeProductFromCart(User currentUser, Scanner scanner) {
        System.out.println("Please select option: 1 - remove item by id; 2 - remove all items; 3 - exit to previous menu:");
        String remove_option = scanner.next();
        switch (remove_option) {
            case "1": {
                System.out.println("Enter product id:");
                int id_to_find = scanner.nextInt();
                boolean idFound = false;
                for (int i = 0; i < currentUser.getUsercart().getCart().size(); i++) {
                    if (currentUser.getUsercart().getCart().get(i).getId() == id_to_find) {
                        System.out.println(currentUser.getUsercart().getCart().get(i).found());
                        currentUser.getUsercart().removeProduct(currentUser.getUsercart().getCart().get(i));
                        System.out.println("Product successfully removed from Your cart!");
                        idFound = true;
                    }
                }
                if (!idFound) {
                    System.err.println("Product with id=" + id_to_find + " was not found!\n");
                }
                break;
            }
            case "2": {
                currentUser.getUsercart().removeAllProducts();
                System.out.println("All products from your cart removed!");
                System.out.println();
                break;
            }
            case "3": {
                break;
            }
            default: {
                System.err.println("Incorrect optio\nn");
                break;
            }
        }
    }

    // Method for confirming order
    public static void confirmOrder(User currentUser){
        if (currentUser.getUsercart().getCart().size() == 0) {
            System.out.println("Item cart is empty - please add products you want to order");
            System.out.println();
        } else {
            System.out.println("Dear " + currentUser.getName() + ", your order:");
            formattedCartPrint(currentUser.getUsercart());
            System.out.println("successfully confirmed!");
            System.out.println();
        }
        currentUser.getUsercart().removeAllProducts();
    }

    // Method for admin's process
    public static void continueAsAdmin(List<Product> products, User currentUser, Scanner scanner){
        System.out.println("Logged in as admin " + currentUser.getName() +" successfully!");
        admin_logged = true;
        try {
            readProductsFromFile(products, "products.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (products.size()>0){
            Product.id_counter = products.get(products.size() - 1).getId();
        }
        while(admin_logged) {
            System.out.println("Please select option:");
            System.out.println("1 - view product list; 2 - add product to product list; 3 - change attributes of product; 4 - remove product from product list; 5 - save changes to DB & log off:");
            String aSelect = scanner.next();
            switch (aSelect) {
                case "1": {
                    //view list of products available
                    formattedProductListPrint(products);
                    System.out.println();
                    break;
                }
                case "2": {
                    // add new product to the shop
                    addProduct(products, scanner);
                    break;
                }
                case "3": {
                    //change product attributes
                    changeProduct(products, scanner);
                    break;
                }
                case "4": {
                    //removing product
                    removeProduct(products, scanner);
                    break;
                }
                case "5": {
                    logOutAsAdmin(products);
                    break;
                }
                default:{
                    System.err.println("Incorrect option\n");
                    break;
                }
            }
        }
    }

    // Method for user's process
    public static void continueAsUser(List<Product> products, List<Product> productsFiltered, User currentUser, Scanner scanner) {
        System.out.println("Logged in as user " + currentUser.getName() + " successfully!");
        try {
            readProductsFromFile(products, "products.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        user_logged = true;
        while (user_logged) {
            System.out.println("Please select option:");
            System.out.println("1 - view current products and filter; 2 - view user cart; 3 - find product and add it to cart; 4 - remove product from cart; 5 - confirm order; 6 - log out");
            String uSelect = scanner.next();
            switch (uSelect) {
                case "1": {
                    //view current products
                    formattedProductListPrint(products);
                    System.out.println();
                    filterWorks(products, productsFiltered, scanner);
                    break;
                }
                case "2": {
                    //view item cart
                    formattedCartPrint(currentUser.getUsercart());
                    System.out.println();
                    break;
                }
                case "3": {
                    //find and add product to item cart
                    searchForProduct(products, currentUser, scanner);
                    break;
                }
                case "4": {
                    //remove product from item cart
                    removeProductFromCart(currentUser, scanner);
                    break;
                }
                case "5": {
                    //confirm order
                    confirmOrder(currentUser);
                    break;
                }
                case "6": {
                    user_logged = false;
                    currentUser.getUsercart().removeAllProducts();
                    products.clear();
                    break;
                }
                default: {
                    System.err.println("Incorrect option\n");
                    break;
                }
            }

        }
    }
}
